import memcache
import memcachedset as mset

profiling = False

def add(mc, index, key):
	mset.add(mc, index, key)
	print "After mset.add [%s]" % key, mc.get(index)

if profiling:
	import cProfile, pstats, StringIO
	pr = cProfile.Profile()
	pr.enable()

mc = memcache.Client(['127.0.0.1:11211'], debug=False)

print "Original:", mc.get('index1')

add(mc, 'index1', 'key1')
add(mc, 'index1', 'key1')
add(mc, 'index1', 'key1')
add(mc, 'index1', 'key2')
add(mc, 'index1', 'key2')

mset.items(mc, 'index1')
print "After mset.items", mc.get('index1')

if profiling:
	pr.disable()
	s = StringIO.StringIO()
	sortby = 'cumulative'
	ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
	ps.print_stats()
	print s.getvalue()