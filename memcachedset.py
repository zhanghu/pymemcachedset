DIRTINESS_THRESHOLD = 3

def encodeSet(keys, op='+'):
    """Encode a set of keys to modify the set.

    >>> encodeSet(['a', 'b', 'c'])
    '+a +b +c '
    """
    return ''.join(op + k + ' ' for k in keys)

def modify(mc, indexName, op, keys):
    encoded = encodeSet(keys, op)
    try:
        append_result = mc.append(indexName, encoded)
        if not append_result and op == '+':
            mc.add(indexName, encoded)
    except KeyError:
        # If we can't append, and we're adding to the set,
        # we are trying to create the index, so do that.
        if op == '+':
            add_result = mc.add(indexName, encoded)
            print "add_result:", add_result

def add(mc, indexName, *keys):
    """Add the given keys to the given set."""
    modify(mc, indexName, '+', keys)

def remove(mc, indexName, *keys):
    """Remove the given keys from the given set."""
    modify(mc, indexName, '-', keys)

def decodeSet(data):
    """Decode an item from the cache into a set impl.

    Returns a dirtiness indicator (compaction hint) and the set

    >>> decodeSet('+a +b +c -b -x')
    (2, set(['a', 'c']))
    """

    keys = set()
    dirtiness = 0
    for k in data.split():
        op, key = k[0], k[1:]
        if op == '+':
            if key in keys:
                dirtiness += 1
            else:
                keys.add(key)
        elif op == '-':
            keys.discard(key)
            dirtiness += 1

    return dirtiness, keys

def item(mc, indexName):
    """Retrieve the current values from the set."""

    data = mc.get(indexName)
    dirtiness, keys = decodeSet(data)
    return keys

def items(mc, indexName, forceCompaction=False):
    """Retrieve the current values from the set.

    This may trigger a compaction if you ask it to or the encoding is
    too dirty."""

    data = mc.gets(indexName)
    dirtiness, keys = decodeSet(data)

    if forceCompaction or dirtiness > DIRTINESS_THRESHOLD:
        compacted = encodeSet(keys)
        mc.cas(indexName, compacted)
    return keys
